# README #

Loudspeaker Health Check by Tom Bland

This plugin applies a tone to the loudspeker output allowing the impedance of the loudspeaker(s) to be measured. The first measurement is the calibration measurment, subsequent measurments are compared to the calibration measurment and should they deviate further than the allowable tollerance a fault is registered.

JSON data files for each plugin instance can be found at CORE_IP_ADDRESS/media/ImpedanceMeasurements/

### How to choose an appropriate frequency ###
There are a number of methods for choosing an apropriate frequency, a rule of thumb is to use an impedance peak, these vary the most as the cabinet or loudspeaker performance changes (usually a low freqency).

On the to-do list is to create a plugin that will do an impedance sweep so that these peaks can be easily found.

### Contact Info ###

* Contact me on the Q-SYS developer network